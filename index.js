//Bài 2
document.getElementById('click__bai2').onclick = function() {
    var thang = document.getElementById('thang').value*1;
    var nam = document.getElementById('nam').value*1;
    var test;
    var ngayTrongThang;
    if(nam%4==0 && nam%100!=0 || nam%400==0) {
        test = true;
    } else {
        test = false;
    }
    switch(thang) {
        case 1: case 3:case 5: case 7:case 8: case 10:case 12: {
            ngayTrongThang = 31;
        } break;
        case 2: {
            if(test === true) {
                ngayTrongThang = 29;
            } else {
                ngayTrongThang = 28;
            }
        } break;
        case 2: case 4:case 6: case 9:case 11: {
            ngayTrongThang = 30;
        } break;
        default: {
            alert('month = 1 --> 12');
        }
    }
    document.getElementById('result__bai2').innerHTML = `Tháng này có ${ngayTrongThang} ngày`
}
//Bài 4
document.getElementById('click__bai4').addEventListener('click', function() {
    var name1 = document.getElementById('sinhVien1').value;
    var name2 = document.getElementById('sinhVien2').value;
    var name3 = document.getElementById('sinhVien3').value;
    var x1 = document.getElementById('x1').value*1;
    var x2 = document.getElementById('x2').value*1;
    var x3 = document.getElementById('x3').value*1;
    var x4 = document.getElementById('x4').value*1;
    var y1 = document.getElementById('y1').value*1;
    var y2 = document.getElementById('y2').value*1;
    var y3 = document.getElementById('y3').value*1;
    var y4 = document.getElementById('y4').value*1;
    var test_4;
    if (x1 >0&& x2 >0  && x3 >0 && x4 >0 && y1 >0 && y2 >0 && y3 > 0 && y4 > 0) {
        test = true;
    } else {
        alert('Tọa độ bạn nhập không đúng');
    }
    var distance_1;
    distance_1 = Math.sqrt((x4-x1)*(x4-x1)+(y4-y1)*(y4-y1));
    var distance_2;
    distance_2 = Math.sqrt((x4-x2)*(x4-x2)+(y4-y2)*(y4-y2));
    var distance_3;
    distance_3 = Math.sqrt((x4-x3)*(x4-x3)+(y4-y3)*(y4-y3));
    if (distance_1 === distance_2 === distance_3) {
        document.getElementById('result__bai4').innerHTML = `Khoảng đường của 3 sinh viên ngang nhau`
    } else {
        if (distance_1 > distance_2 && distance_1 >distance_3 ) {
            document.getElementById('result__bai4').innerHTML = `Sinh viên ${name1} xa nhất`
        } else if (distance_2 > distance_1 && distance_2 >distance_3) {
            document.getElementById('result__bai4').innerHTML = `Sinh viên ${name2} xa nhất`
        } else {
            document.getElementById('result__bai4').innerHTML = `Sinh viên ${name3} xa nhất`
        }
    }
})